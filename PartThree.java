import java.util.Scanner;
public class PartThree{
  public static void main (String[] args){
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter value 1: ");
    double x = sc.nextInt();
    
    System.out.println("Enter value 2: ");
    double y = sc.nextInt();
    
    Calculator scan = new Calculator();
    double add = scan.add(x,y);
    System.out.println("added: "+add);
    
    double substract = scan.substract(x,y);
    System.out.println("substracted: "+substract);
    
    double multiply = scan.multiply(x,y);
    System.out.println("multiplied: "+multiply);
    
    double divide = scan.divide(x,y);
    System.out.println("divided: "+divide);
  }
}
