public class MethodsTest{
 public static void main(String[] args){
  int x = 5;
  int j = 5;
  double y = 7;
  System.out.println(x);
  methodNoInputNoReturn();
  System.out.println(x);
  
  System.out.println(x);
  methodOneInputNoReturn(x+10);
  System.out.println(x);
  
  methodTwoInputNoReturn(x,y);
  
  int z = methodNoInputReturnInt();
  System.out.println(z);
  
  double v = sumSquareRoot(9, 5);
  System.out.println(v);
  
  String s1 = "java";
  String s2 = "programming";
  System.out.println("length of s1 is: "+s1.length());
  System.out.println("length of s2 is: "+s2.length());
  
  System.out.println("Add 1 in original num: "+SecondClass.addOne(50));
  
  SecondClass sc = new SecondClass();
  int add2 = sc.addTwo(50);
  System.out.println("Add 2 in original num: "+add2);
  
 }
 public static void methodNoInputNoReturn(){
  System.out.println("I’m in a method that takes no input and returns nothing");
  int x = 20;
  System.out.println(x);
 }
 public static void methodOneInputNoReturn(int x){
  x-=5;
  System.out.println("Inside the method one input no return");
  System.out.println(x);
 }
 public static void methodTwoInputNoReturn(int x, double y){
  System.out.println("Inside the method 2 input no return");
  System.out.println(x);
  System.out.println(y);
 }
 public static int methodNoInputReturnInt(){
   System.out.println("Inside the method no input, but return int");
   return 5;
 }
 public static double sumSquareRoot(int x, int j){
   System.out.println("Inside the method sumSquareRoot");
   int sum = x+j;
   double result = Math.sqrt(sum);
   
   return result;
 }
}